AC_INIT(gio/gfile.h)

AM_INIT_AUTOMAKE(gio-standalone, 0.1.3)
AM_CONFIG_HEADER(config.h)
	
AM_SANITY_CHECK

AC_C_CONST
AC_ISC_POSIX
AC_PROG_CC
AC_PROG_CPP
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET
AM_DISABLE_STATIC
AM_PROG_LIBTOOL
AC_PATH_PROG(PKG_CONFIG, pkg-config, no)
AM_PROG_CC_C_O

AC_TYPE_PID_T
AC_TYPE_SIGNAL
AC_TYPE_SIZE_T
AC_TYPE_UID_T

AH_VERBATIM([_GNU_SOURCE],
[/* Enable GNU extensions on systems that have them.  */
#ifndef _GNU_SOURCE
#  define _GNU_SOURCE
#endif])
	
saved_CFLAGS=$CFLAGS
AC_CHECK_MEMBERS([struct stat.st_mtimensec, struct stat.st_mtim.tv_nsec, struct stat.st_atimensec, struct stat.st_atim.tv_nsec, struct stat.st_ctimensec, struct stat.st_ctim.tv_nsec])
AC_CHECK_MEMBERS([struct stat.st_blksize, struct stat.st_blocks])
CFLAGS=$saved_CFLAGS

AC_CHECK_HEADERS([pwd.h grp.h])

AC_CHECK_FUNCS(getc_unlocked readlink symlink chown lchown fchmod fchown link statvfs statfs utimes getgrgid getpwuid)

AC_CHECK_HEADERS(sys/vfs.h sys/mount.h sys/statfs.h sys/statvfs.h sys/param.h)
			     
dnl
dnl if statfs() takes 2 arguments or 4 (Solaris)
dnl
if test "$ac_cv_func_statfs" = yes ; then
  AC_MSG_CHECKING([number of arguments to statfs()])
  AC_TRY_COMPILE([#include <unistd.h>
  #ifdef HAVE_SYS_PARAM_H
  #include <sys/param.h>
  #endif
  #ifdef HAVE_SYS_VFS_H
  #include <sys/vfs.h>
  #endif
  #ifdef HAVE_SYS_MOUNT_H
  #include <sys/mount.h>
  #endif
  #ifdef HAVE_SYS_STATFS_H
  #include <sys/statfs.h>
  #endif], [struct statfs st;
  statfs(NULL, &st);],[
    AC_MSG_RESULT([2])
    AC_DEFINE(STATFS_ARGS, 2, [Number of arguments to statfs()])],[
    AC_TRY_COMPILE([#include <unistd.h>
  #ifdef HAVE_SYS_PARAM_H
  #include <sys/param.h>
  #endif
  #ifdef HAVE_SYS_VFS_H
  #include <sys/vfs.h>
  #endif
  #ifdef HAVE_SYS_MOUNT_H
  #include <sys/mount.h>
  #endif
  #ifdef HAVE_SYS_STATFS_H
  #include <sys/statfs.h>
  #endif], [struct statfs st;
  statfs(NULL, &st, sizeof (st), 0);],[
      AC_MSG_RESULT([4])
      AC_DEFINE(STATFS_ARGS, 4, [Number of arguments to statfs()])],[
      AC_MSG_RESULT(unknown)
      AC_MSG_ERROR([unable to determine number of arguments to statfs()])])])
fi
			     
dnl Volume monitor stuff			     
AC_CHECK_FUNCS(setmntent endmntent hasmntopt getmntinfo)
AC_CHECK_HEADERS(mntent.h sys/mnttab.h sys/vfstab.h sys/cdio.h sys/mount.h sys/mntctl.h sys/vfs.h sys/vmount.h sys/sysctl.h fstab.h fnmatch.h util.h sys/sysmacros.h)
			     
AC_MSG_CHECKING([for Win32])
case "$host" in
  *-*-mingw*)
    glib_native_win32=yes
    ;;
  *)
    glib_native_win32=no
    ;;
esac

AC_MSG_RESULT([$glib_native_win32])
AM_CONDITIONAL(OS_WIN32, [test "$glib_native_win32" = "yes"])
AM_CONDITIONAL(OS_UNIX, [test "$glib_native_win32" != "yes"])

GTK_DOC_CHECK
DISTCHECK_CONFIGURE_FLAGS="--enable-gtk-doc"
AC_SUBST(DISTCHECK_CONFIGURE_FLAGS)

PKG_CHECK_MODULES(GLIB, glib-2.0 >= 2.13.8 gthread-2.0 gobject-2.0 gmodule-no-export-2.0)
AC_SUBST(GLIB_CFLAGS)
AC_SUBST(GLIB_LIBS)

GETTEXT_PACKAGE=gio
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [The gettext domain name])
AC_SUBST(GETTEXT_PACKAGE)

AM_GLIB_GNU_GETTEXT

dnl ****************************
dnl *** Check for libselinux ***
dnl ****************************
AC_ARG_ENABLE(selinux, [  --disable-selinux       build without selinux support])
msg_selinux=no
SELINUX_LIBS=
if test "x$enable_selinux" != "xno"; then

 AC_CHECK_LIB(selinux, is_selinux_enabled,
   [AC_CHECK_HEADERS(selinux/selinux.h,
     [AC_SEARCH_LIBS(lgetfilecon_raw, selinux, 
       [AC_DEFINE(HAVE_SELINUX, 1, [Define to 1 if libselinux is available])
        SELINUX_LIBS="-lselinux"
        msg_selinux=yes])
     ])
   ])
fi
AC_SUBST(SELINUX_LIBS)

dnl ***********************
dnl ** Check for inotify **
dnl ***********************
inotify_support=no
AC_CHECK_HEADERS([linux/inotify.h],
[
	inotify_support=yes
])
AC_CHECK_HEADERS([sys/inotify.h],
[
	inotify_support=yes
])

AM_CONDITIONAL(HAVE_INOTIFY, [test "$inotify_support" = "yes"])

dnl **********************
dnl *** Checks for FAM ***
dnl **********************

should_disable_fam=no

AC_ARG_ENABLE(fam, [  --disable-fam          build without enabling fam for file system monitoring],
                         [
                                if test "x$enable_fam" = "xno"; then
                                        should_disable_fam=yes
                                        echo "Not building FAM support"
                                fi
                         ]
                         )
fam_support=no
FAM_LIBS=
if test "x$should_disable_fam" = "xno"; then
AC_CHECK_LIB(fam, FAMOpen,
  [AC_CHECK_HEADERS(fam.h,
    [AC_DEFINE(HAVE_FAM, [], [Define if we have FAM])
     AC_CHECK_LIB(fam, FAMNoExists,
     		  AC_DEFINE(HAVE_FAM_NO_EXISTS, [], [Define if we have FAMNoExists in fam]))
     FAM_LIBS="-lfam"]
     fam_support=yes,
    AC_MSG_WARN(*** FAM support will not be built (header files not found) ***))],
  AC_MSG_WARN(*** FAM support will not be built (FAM library not found) ***))
AC_SUBST(FAM_LIBS)
fi
AM_CONDITIONAL(HAVE_FAM, [test "$fam_support" = "yes"])


dnl ***********************
dnl *** Check for xattr ***
dnl ***********************
AC_ARG_ENABLE(xattr, [  --disable-xattr           build without xattr support])
msg_xattr=no
XATTR_LIBS=
if test "x$enable_xattr" != "xno"; then

dnl either glibc or libattr can provide xattr support

dnl for both of them, we check for getxattr being in
dnl the library and a valid xattr header.

dnl try glibc
 AC_CHECK_LIB(c, getxattr,
   [AC_CHECK_HEADERS(sys/xattr.h,
     [AC_DEFINE(HAVE_XATTR, 1, [Define to 1 if xattr is available])
      msg_xattr=yes])
   ])

  if test "x$msg_xattr" != "xyes"; then
dnl   failure. try libattr
   AC_CHECK_LIB(attr, getxattr,
      [AC_CHECK_HEADERS(attr/xattr.h,
       [AC_DEFINE(HAVE_XATTR, 1, [Define to 1 if xattr is available])
        XATTR_LIBS="-lattr"
        msg_xattr=yes])
      ])
  fi
fi
AC_SUBST(XATTR_LIBS)

dnl ==========================================================================
	
dnl Globally define_GNU_SOURCE and therefore enable the GNU extensions
AC_DEFINE(_GNU_SOURCE, 1, [Enable GNU Extensions])

dnl ==========================================================================
	
AC_DEFINE(_FILE_OFFSET_BITS, 64, [Enable LFS])
		       
dnl ==========================================================================

AC_PATH_PROG(GLIB_GENMARSHAL, glib-genmarshal)


dnl check for mt safe function variants and some posix functions
dnl ************************************************************

glib_save_LIBS="$LIBS"
if test "$ac_cv_header_pwd_h" = "yes"; then
 	AC_CACHE_CHECK([for posix getpwuid_r],
			ac_cv_func_posix_getpwuid_r,
			[AC_TRY_RUN([
#include <errno.h>
#include <pwd.h>
int main () { 
    char buffer[10000];
    struct passwd pwd, *pwptr = &pwd;
    int error;
    errno = 0;
    error = getpwuid_r (0, &pwd, buffer, 
                        sizeof (buffer), &pwptr);
   return (error < 0 && errno == ENOSYS) 
	   || error == ENOSYS; 
}                       ],
			[ac_cv_func_posix_getpwuid_r=yes],
			[ac_cv_func_posix_getpwuid_r=no])])
	if test "$ac_cv_func_posix_getpwuid_r" = yes; then
		AC_DEFINE(HAVE_POSIX_GETPWUID_R,1,
			[Have POSIX function getpwuid_r])
	else
 		AC_CACHE_CHECK([for nonposix getpwuid_r],
			ac_cv_func_nonposix_getpwuid_r,
			[AC_TRY_LINK([#include <pwd.h>],
                               	[char buffer[10000];
                               	struct passwd pwd;
                               	getpwuid_r (0, &pwd, buffer, 
                                       		sizeof (buffer));],
				[ac_cv_func_nonposix_getpwuid_r=yes],
				[ac_cv_func_nonposix_getpwuid_r=no])])
		if test "$ac_cv_func_nonposix_getpwuid_r" = yes; then
			AC_DEFINE(HAVE_NONPOSIX_GETPWUID_R,1,
				[Have non-POSIX function getpwuid_r])
		fi
	fi
fi
if test "$ac_cv_header_grp_h" = "yes"; then
 	AC_CACHE_CHECK([for posix getgrgid_r],
			ac_cv_func_posix_getgrgid_r,
			[AC_TRY_RUN([
#include <errno.h>
#include <grp.h>
int main () { 
    char buffer[10000];
    struct group grp, *grpptr = &grp;
    int error;
    errno = 0;
    error = getgrgid_r (0, &grp, buffer, 
                        sizeof (buffer), &grpptr);
   return (error < 0 && errno == ENOSYS) 
	   || error == ENOSYS; 
}                       ],
			[ac_cv_func_posix_getgrgid_r=yes],
			[ac_cv_func_posix_getgrgid_r=no])])
	if test "$ac_cv_func_posix_getgrgid_r" = yes; then
		AC_DEFINE(HAVE_POSIX_GETGRGID_R,1,
			[Have POSIX function getgrgid_r])
	else
 		AC_CACHE_CHECK([for nonposix getgrgid_r],
			ac_cv_func_nonposix_getgrgid_r,
			[AC_TRY_LINK([#include <grp.h>],
                               	[char buffer[10000];
                               	struct group grp;	
                               	getgrgid_r (0, &grp, buffer, 
                                       		sizeof (buffer));],
				[ac_cv_func_nonposix_getgrgid_r=yes],
				[ac_cv_func_nonposix_getgrgid_r=no])])
		if test "$ac_cv_func_nonposix_getgrgid_r" = yes; then
			AC_DEFINE(HAVE_NONPOSIX_GETGRGID_R,1,
				[Have non-POSIX function getgrgid_r])
		fi
	fi
fi
LIBS="$glib_save_LIBS"


dnl ==========================================================================
dnl Turn on the additional warnings last, so -Werror doesn't affect other tests.

AC_ARG_ENABLE(more-warnings,
[  --enable-more-warnings  Maximum compiler warnings],
set_more_warnings="$enableval",[
if test -f $srcdir/CVSVERSION; then
	is_cvs_version=true
	set_more_warnings=yes
else
	set_more_warnings=no
fi
])
AC_MSG_CHECKING(for more warnings, including -Werror)
if test "$GCC" = "yes" -a "$set_more_warnings" != "no"; then
	AC_MSG_RESULT(yes)
	CFLAGS="\
	-Wall \
	-Wchar-subscripts -Wmissing-declarations -Wmissing-prototypes \
	-Wnested-externs -Wpointer-arith \
	-Wcast-align -Wsign-compare \
	$CFLAGS"

	for option in -Wno-strict-aliasing -Wno-sign-compare; do
		SAVE_CFLAGS="$CFLAGS"
		CFLAGS="$CFLAGS $option"
		AC_MSG_CHECKING([whether gcc understands $option])
		AC_TRY_COMPILE([], [],
			has_option=yes,
			has_option=no,)
		if test $has_option = no; then
			CFLAGS="$SAVE_CFLAGS"
		fi
		AC_MSG_RESULT($has_option)
		unset has_option
		unset SAVE_CFLAGS
	done
	unset option
else
	AC_MSG_RESULT(no)
fi
			       
AC_OUTPUT([
Makefile
gio/Makefile
docs/Makefile
docs/reference/Makefile
docs/reference/gio/Makefile
docs/reference/gio/version.xml
gio/xdgmime/Makefile
gio/inotify/Makefile
gio/fam/Makefile
programs/Makefile
po/Makefile.in
gio-2.0.pc
gio-unix-2.0.pc
])

echo 
echo "gio configuration summary:"
echo "
	Win32 build:          $glib_native_win32
	SELinux support:      $msg_selinux
	xattr support:        $msg_xattr
	inotify support:      $inotify_support
	FAM support:          $fam_support
"
